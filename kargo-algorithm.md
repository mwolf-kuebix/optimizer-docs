---
title: KARGO Optimization Algorithm Design Proposal
output: pdf_document
---

# Purpose

This document is a proposal to implement an "Optimizer 2.0" routing algorithm. With the current optimization implementaion, there have been significant design pitfalls in attempting to expand the algorithm, shortcomings with performance, and design compromises to handle routing constraints which have lead to the current implementation becoming in the opinion of the current engineers assigned to it, a tar pit worth deprecating.

The goal of this document is to be high level yet technical in the style of a whitepaper, so that business and non-software decision makers may be informed of the engineering decision and participate in feedback.

\tableofcontents

# Shortcomings of Existing Implementation

## Performance

With newly discovered requirements, the performance of the existing implementation has become too poor to continue use. At least 1500 shipments per problem instance are required to be handled in a reasonable amount of time, and the current algorithm hangs under such load.

## API Design

The current algorithm's API is strongly coupled to the JSON API. a refactor to create a more idiomatic java API would not be backwards compatible due to this, and API changes are incredibly difficult to propagate to all current optimization users.

## Algorithm Design

TODO

# New Algorithm Design Proposal

This section shall describe the proposed new algorithm and provide the reader with a non-exhaustive background on the field.

## Metaheuristic High Level Description

This section describes at a high level what a metaheuristic algorithm is, since it is important to recognize the nuances differentiating between exact algorithms, heuristic algorithms, and metaheuristic algorithms.

### Exact Algorithm

An exact algorithm is an algorithm guaranteeing its output to be a globally optimal solution to the optimization problem at hand. An example of an exact algorithm is the A\* search algorithm. Exact solutions explore the entirety of the search space of the optimization problem, which is the only known way to guarantee the discovery of a globally optimal solution for sufficiently complex problems.

### Heuristic Algorithms

There is a distinction to be made between heuristic algorithms and approximation algorithms in the literature, but for the purposes of this document these differences are judged to be irrelevant. A heuristic algorithm may be classified into two families: *specific heuristics* and *metaheuristics*.
Specific heuristics are designed to solve a specific instance or class of instances of problems. Metaheuristics are general-purpose algorithms capable of being applied to solve almost any optimization problem. They may be viewed as upper level methodoligies that guide the strategy and design of underlying heuristics to solve specific optimization problems.

Metaheuristics serve three main purposes: solving problems faster, solving large problems, and creating robust algorithms. They can also be simpler to design and implement and offer greater flexibility than the other aforementioned classes of algorithms.

The particular metaheuristic suggested for implementation is named *Tabu Search*.

## Tabu Search

Tabu Search (TS) was conceived of by Fred Glover in 1986. Around that time there was a miniature renaissance in the field. It was inspired by the high level idea of the Simulated Annealing (SA) metaheuristic. SA uses controlled randomization to escape from locally optimal candidate solutions which are pitfalls for purely greedy local search algorithms. Other researchers proposed similar (in high level goal) algorithms such as *steepest ascent/mildest descent* which attempt to solve the same problem of guiding the algorithm away from locally optimal solutions within the search space. Out of the algorithms proposed by researchers during this time, TS gained the most popularity due to its performance and simplicity in design relative to its capability in being adapted to problems.

TS behaves like a steepest local search algorithm (aka a greedy algorithm), but accepts non-improving solutions to escape from a locally optimal solution when all neighboring solutions are nonimproving. To avoid cycles, TS has a memory of recently-explored solution candidates which are "tabu" (hence the name tabu search) and cannot be revisited until they are removed from the tabu list. There are other implementation details which can be layered into the TS algorithm to improve solution quality, but this is the high-level suitable for the audience of this paper and a nearly-bare-bones implementation of TS similar to Glover's originally proposed algorithm is judged to be suitable for rapid prototyping for proof of concept, and even initial implementation in production.

## Specific Benefits of Tabu Search

This section addresses the benefits of TS compared to the current routing algorithm.

The existing implementation is a greedy algorithm. This means that it is guaranteed to be stymied when it finds locally optimal solutions. It attempts to diversify the searched solution space in some ways but these are bandaids over the underlying issue of the algorithm's greediness. these diversification methods ended up causing unacceptable performance issues with the development of new input and requirements which is why we are searching for a new algorithm.

Metaheuristics in general are designed to overcome this pitfall, as mentioned earlier.

TODO: enumerate other benefits

## Tabu Search Vs. Other Candidate Algorithms

This section compares Tabu Search to other popular choices for algorithms to give the reader an idea of the thought process of why TS was chosen for this proposal.

A brief survey was conducted of popular algorithms used for VRP issues (see *Vehicle Routing, Problems, Methods, and Applications* by Toth and Vigo, and *Metaheuristics From Design to Implementation* by Talbi) and TS stood out as a candidate for several reasons. It is familiar to the Blackhawk team due to previous industry experience, there exist readily available open source java libraries to aid in the implementation of a more tailored TS which have been used in industry to great success, and it is one of the most conceptually simple metaheuristics of the popular ones used today. While it is not the newest or most performant star of contemporary combinatorial optimization and vehicle routing, TS is one of the simplest, while being judged to be close enough in performance for our VRP solving needs to newer proposed algorithms. TS has been used to great success in both design and control problems in different industries, so it is suitable for our needs as we are still really feeling out whether we require this optimizer to be more design-oriented (problem solved once, high quality solution required, time to solve/run algorithm less critical) or a control issue (problem updates frequently in real-time, causing needs for re-solving a new problem and therefore rerunning the algorithm. Control problems are also known as operational problems.


# Implementation Design

## Initial Scenario generation

Most metaheuristics are most performant by "seeding" the algorithm with a pre-created complete solution. To generate this, it is suggested to use the current implementation without any of its diversification hooks which have hit performance so hard recently. This is a temporary solution until a more general constructive algorithm can be written for this initial scenario. It is also common practice to generate a random solution regardless of validity and allow TS to iterate from that.

A later solution to this is recommended to perform k-means clustering on the locations of pickup and delivery tasks, and then create initial routes based on the k-means clustering results.


## Candidate Solution Format

The actual representation of the data iterated on is critical for efficiency. it is recommended that candidate solutions be represented as a permutation of a set of integers, where each integer represents a location stopped at on the route. This allows a large support of many locations, while being a compact data format.

## Encoding/Decoding

The aforementioned information is obviously not a complete solution. There is a myriad of data including timing and assigned tasks missing from that data.

After the core TS generates an iteration on a candidate, this candidate is then *decoded* by another module of software: this software is responsible for examining the generated route and determining if the route may be assigned a task based on its visited locations. Then the route and its complete tasking is scored for cost and value based on assigned tasks and cost to travel to conduct assigned tasks, and its final score is compared to other candidate solutions. It is in and of itself suitable to consider running another metaheuristic inside the decoding algorithm to generate assigned tasks, but this is TBD.

## Objective Function

The objective function is the computation which scores the decoded solution. This algorithm should be SOFT - this means that it is allowed to violate any constraints at a penalty of added cost. A Soft objective function can be used to simulate a HARD algorithm which rejects candidates based on failing to meet constraints by simply raising the constraint violation cost sufficiently high.


