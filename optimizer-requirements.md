---
title: "Optimizer Engine Software Requirements"
author: 
date: July 22, 2020
geometry: margin=2cm
output:
pdf_document:
toc: true
toc_depth: 2
---

\pagebreak

# SCOPE
This document describes the requirements for an optimization engine (The Software). This document shall provide a set of requirements to be given to a software engineer unfamiliar for the most part with vehicle routing and shipment optimization, from which they may produce a software design.

This document does not apply to user interfaces or displays, only the data required to be modeled and considered by The Software and the required behavior of the implementation of The Software.

\pagebreak

# Definitions
The definitions here are in alphabetical order within each section.

## General Definitions
The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in RFC 2119.

## Consolidation
Consolidation is the process by which packages are allocated onto vehicles. It is synonymous with "assignment" or "allocation," and the computer science literature favors the use of those terms. Consolidation is a discrete sub-problem of the optimization problem which MAY be considered on its own by a consolidation algorithm or by a hollistic optimization algorithm.

## Engine
The Engine is a synonym to The Software.

## Optimization Problem (OP)
The optimization problem is the theoretical hollistic problem with all considerations included. a non-exaustive list of these considerations in no particular order are: Shipment Consolidation Pickup and Delivery with time windows Driver Hours Distance and Travel Time * Shipment uncertainty

## Routing
Routing is the process of planning vehicle routes with the goal of transporting orders from their starting location to their planned destinations. Routing is a discrete sub-problem of the optimization problem which may be considered on its own by a routing algorithm or by a hollistic optimization algorithm.

### Shipping Lanes
A Shipping lane is a cost per mile variable defined by the start and end point of a route. Instead of having a simple cost per mile no matter where the drive, a vehicle may have a variable rate based on where it starts and ends.

## Scenario
A scenario is a valid set of input data with which The Software can optimize.

## Initial Scenario
An initial scenario is a scenario indicated by the end user of The Software to be the start from which the algorithm will attempt to optimize.

## Vehicles
### Vehicle
A vehicle is a device with which a driver transports orders from their starting location to their destination. A vehicle has a size which MUST be respected when assigning packages to vehicles.

## Locations
### Depot
A Depot is a location from which vehicles MAY be drawn. Depots have MAY have any number of vehicles. There MAY also be multiple depots from which vehicles may be drawn.

### Location
A Location is a place referenced by an address or a corresponding latitude/longitude.

## Orders
### Allocation / assignment
An assignment describes the relationship between a vehicle and an order during the consolidation process. If an order is assigned to a vehicle by The Software, then that vehicle will at some point during its route pickup and deliver that order.

### Available Order
An Available order is an order which is ready to ship and MAY be edited/managed/consolidated by The Software.

### Order
An Order describes a physical item with pickup and delivery requirements. An order MAY start either at a location (the pickup location), or with a fixed assignment to a truck. An Order

### Load
A load is a shipment with an undefined carrier, BOL#, and contract cost.

### Requirements, Pickup/Delivery
Pickup and Delivery requirements are constraints regarding the pickup and delivery of an individual order. Some examples of these are:

* Daily time windows/hours of operation of the location to pick up from or drop off at.
* AM/PM windows
* a "multi-day" window, for example, April 4th to April 5th
* Hours of operation in in conjunction with valid dates, such as " April 4th, 9 AM-6PM"

### Shipment
A Shipment is an order which has been assigned to a vehicle for transportation from its starting location to its delivery location.

\pagebreak

# Requirements
## Required States and Modes
No states or modes are required.

# System Capability Requirements
## Input Processing Subsystem
The Software MUST accept valid partial input. For example, the end user might submit a vehicle type specification, then later submit orders and then a final scenario instructing The Software to use that vehicle type for routing.

## Routing Subsystem
* The Software must provide a subsystem to compute routes for multiple vehicles which pickup and deliver packages in accordance with the submitted constraints.

### Costing strategy requirements
* The Software MUST support a per-vehicle-type cost-per-mile costing strategy.
* The Software MUST support a lane-based costing strategy
* The Software MUST support per-run configuration of the costing strategy.

### Constraint Requirements
* The Software MUST support distance-based clustering of shipments.
* The Software MUST support a maximum stops per route.
* The Software MUST support a maximum "out of route" distance.
* The Software MUST support a minimum stem percentage.
* The Software MUST support a minimum capacity picked up per pickup stop.
* The Software MUST support a minimum capacity dropped off per stop.
* The Software MUST support a minimum capacity serviced per route.
* The Software MUST support a maximum route time.
* The Software MUST support a maximum route distance.
* The Software MUST support a flat duration per pickup/delivery stop.
* The Software MUST support a per-shipment pickup duration.
* The Software MUST support a per-shipment dropoff duration.

## External Interface Identification
This section contains the description of all types of data which must be modeled by the external API of The Software.

## Scenario
* A scenario MUST be associated with one or more vehicles.
* A scenario MUST be associated with one or more available orders.

## Location
* A Location MUST be associated with a valid street address and zipcode which can be geolocated to a latitude and longitude, OR a valid latitude/longitude.
* A Location MAY be associated with an "Hours of Operation"

## Order Requirements
* An Order MUST be associated with a pickup location.
* An Order MUST be associated with a dropoff location.
* An Order MAY be associated with a pickup time window. If it is, this time window MUST be a valid subset of its pickup location's Hours of Operation.
* An Order MAY be associated with a dropoff time window. If it is, this time window MUST be a valid subset of its dropoff location's Hours of Operation.
* An Order MAY be associated with a required temperature range. All orders with temperature range MUST be assigned with temperature-compatible
* An Order MUST be associated with a required truck Capacity for loading.
* The Software MUST NOT command a vehicle to pickup any orders exceeding the remaining size of the vehicle.

## Vehicle Requirements
* A Vehicle MUST be associated with a Capacity.
* A Vehicle MUST be associated with a start location.
* A Vehicle MAY be associated with an end location.
* The Software MUST route all vehicles with end locations to their end location as the final leg of their route.
* A Vehicle MUST be associated with a driver hours/break model
Driver Hours Requirements
* A Driver Hour Model MUST be associated with a boolean flag indicating whether overnight routes are valid.
* A Driver Hour Model MUST be associated with a drive time until short break length.
* A Driver Hour Model MUST be associated with a total drive time per day.
* A Driver Hour Model MUST be associated with a short break length in seconds.
* A Driver Hour Model MUST be associated with a long break length in seconds.

## Capacity Requirements

* The Software MUST support solid capacities.
* The Software MUST support liquid capacities.

### Solid Capacity Requirements
* Sizes MUST be associated with a number of pallets.
* Sizes MUST be associated with a number of cubes.
* Sizes MUST be associated with a weight.

### Liquid Capacity Requirements

TODO

## Routing Constraint Configuration Requirements
* The Software MUST consider all submitted constraints when generating routes.
* The Software MUST accept a constraint to configure the maximum number of stops per route.
* The Software MUST accept a constraint to configure the minimum size which must be picked up per pickup stop.
* The Software MUST accept a constraint to configure the minimum size which must be dropped off per dropoff stop.


## Network Partner Requirements

TODO
