---
title: Coding Standards for KARGO Optimizer
author: Matt Wolf
output: 
	pdf_document:
		number_sections: true
---

\tableofcontents

# Purpose

This document is a proposal for coding standards for all software products worked by the blackhawk team, and the code review process for that software.

# Definitions

[The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED",  "MAY", and "OPTIONAL" in this document are to be interpreted as described in RFC 2119](https://www.ietf.org/rfc/rfc2119.txt).

# Coding Standards

## Java

### Overall Style Guide

Unless contradicted by this document, the [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html) MUST be followed for all code written in Java developed for KARGO.

### Naming

All names SHOULD strike a balance between descriptive and overly verbose, with a tendency towards descriptive. Horizontal line space is cheap, code that is not understandable is expensive to ramp new people up on.

If a variable or method name represents a value associated with a unit of measure, that name MUST have appended to it the unit of measure in which the data is represented. The appended unit name removes all ambiguity while avoiding adding additional variables to communicate the same information. Additional variables have a (minute) performance and memory cost, an extra few characters on a variable name does not.

### Naming Exception: Variable Names for Academic or Textbook Algorithms

When implementing a textbook algorithm verbatim from literature, mathematical variable names are acceptable as long as they are given javadoc describing their purpose. The source code is first and foremost written to be re-read by future software engineers, and SHOULD serve as a mini guide on the subject area for them.

### Guidance for Simplicity

Each line SHOULD generally try to do a single thing in source code. it is our job as engineers to write readable code, and digestible simplicity is important for readability, it is the compiler and JVM's job to optimize. Performance should be considered secondary to readability and heavy profiling of implementation code should only be conducted after it is clear the implementation is slower than desired by testers. This section does not allow the lack of good algorithmic design - the time complexity of the software MUST be discussed and ideally analyzed by team members. it is appropriate to mention the time complexity of methods in their javadoc.

More broadly, all aspects and modules of the software should try to do just *one* thing. This is also known as the separation of responsibilities. This can of course become hairy when that *one thing* is providing REST endpoints for a backend service, but that is an acceptable *one thing* to do - that is a web service layer on top of a backend library. Every method, class, even every line of code SHOULD generally do *one thing* even if that one thing is somewhat complex. Adherring to this principle aids readability and ease of understanding the software.

### SOLID

The above paragraph can be considered an application of the SOLID principles' first principle, *single-responsibility*. It is the opinion of the author that simplicity and the simplification of complex problems into bite-size is the most important and that to a large degree, the other SOLID principles more or less may be derived from the idea of radical simplicity in software. [The SOLID principles can be found on wikipedia at the time of the writing.](https://en.wikipedia.org/wiki/SOLID)

SOLID principles SHOULD be adhered to at all levels of design and implementation for production software. Strong reasoning must be given for violating SOLID principles.
