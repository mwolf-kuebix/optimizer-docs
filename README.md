# README #

### What is this repository for? ###

This repository contains the plaintext source files containing the KARGO software requirements and design documents, and any other useful software documentation.

### How do I get set up? ###

This repository has only been tested thus far on a linux system so the tools used are up to the user of this repo to check out, for the time being.

This repository contains .puml files and .md files, which are the source code of this repository. they are both plaintext file extensions, plantUML and markdown respectively.

PlantUML files are compiled by the plantuml jar executable or other IDE extension providing plantUML compilation functionality to turn into various UML diagrams. The plantUML jar and other addons for working with plantuml files can be found at [https://plantuml.com/](https://plantuml.com/)

.md files are Markdown files. These are to be compiled with pandoc into PDF or HTML documents for general distribution. A tutorial on pandoc is left to the reader to peruse the man pages or look up the excellent tutorials by Luke Smith on Youtube or at [lukesmith.xyz](https://lukesmith.xyz)


### Contribution guidelines ###

Submit a PR. Do not for the time being submit commits with end-result files such as pdfs. Only the source files.

### Who do I talk to? ###

email mwolf@kuebix.com
